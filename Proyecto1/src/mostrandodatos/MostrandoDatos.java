package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
		Scanner teclado=new Scanner(System.in);
		System.out.println("Introduce un nombre:");
		String nombre=teclado.nextLine();
		System.out.println("Introduce un apellido:");
		String apellido=teclado.nextLine();
		
		System.out.println("El nombre es: "+nombre);
		System.out.println("El apellido es: "+apellido);
		teclado.close();
	}

}
